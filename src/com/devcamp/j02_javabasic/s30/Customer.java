package com.devcamp.j02_javabasic.s30;

public class Customer {
    byte myByte;//byte
    short myShortNumber;//short
    long myLongNumber;//long
    double myDoubleNumber;//double
    int myIntNumber;// integer(whole number)
    float myFloatNumber;// floating point number
    char myLetter;//character
    boolean myBool;//boolean
    
    public Customer(){
        myByte = 127;
        myShortNumber = 2022;
        myLongNumber = 2021;
        myDoubleNumber = 123.4;
        myIntNumber = 5;
        myFloatNumber = 5;
        myLetter ='P';
        myBool = false;
    }
    public Customer(byte byteNum, short shortNum, long longNum, double doubleNum, int intNum, float floatNum, char letter, boolean bool){
        myByte = byteNum;
        myShortNumber = shortNum;
        myLongNumber = longNum;
        myDoubleNumber = doubleNum;
        myIntNumber = intNum;
        myFloatNumber = floatNum;
        myLetter =letter;
        myBool = bool;
    }

    public static void main(String[] args) throws Exception {
        Customer customer = new Customer();
        System.out.println(customer.myByte);
        System.out.println(customer.myShortNumber);
        System.out.println(customer.myLongNumber);
        System.out.println(customer.myDoubleNumber);
        System.out.println(customer.myIntNumber);
        System.out.println(customer.myFloatNumber);
        System.out.println(customer.myLetter);
        System.out.println(customer.myBool);
        System.out.println("Run 1: ");
        byte myByte = 10;
        short myShort = 2001;
        customer = new Customer(myByte, myShort,7001,171.9,231,71.99f,'A',false);
        System.out.println(customer.myByte);
        System.out.println(customer.myShortNumber);
        System.out.println(customer.myLongNumber);
        System.out.println(customer.myDoubleNumber);
        System.out.println(customer.myIntNumber);
        System.out.println(customer.myFloatNumber);
        System.out.println(customer.myLetter);
        System.out.println(customer.myBool);
        System.out.println("Run 2: ");
        myByte = 20;
        myShort = 2002;
        customer = new Customer(myByte, myShort,7002,52.9,465,45.25f,'B',true);
        System.out.println(customer.myByte);
        System.out.println(customer.myShortNumber);
        System.out.println(customer.myLongNumber);
        System.out.println(customer.myDoubleNumber);
        System.out.println(customer.myIntNumber);
        System.out.println(customer.myFloatNumber);
        System.out.println(customer.myLetter);
        System.out.println(customer.myBool);
        System.out.println("Run 3: ");
        myByte = 30;
        myShort = 2003;
        customer = new Customer(myByte, myShort,7003,15.8,123,52.17f,'C',false);
        System.out.println(customer.myByte);
        System.out.println(customer.myShortNumber);
        System.out.println(customer.myLongNumber);
        System.out.println(customer.myDoubleNumber);
        System.out.println(customer.myIntNumber);
        System.out.println(customer.myFloatNumber);
        System.out.println(customer.myLetter);
        System.out.println(customer.myBool);
        System.out.println("Run 4: ");
        myByte = 40;
        myShort = 2004;
        customer = new Customer(myByte, myShort,7004,23.8,789,56.19f,'D',true);
        System.out.println(customer.myByte);
        System.out.println(customer.myShortNumber);
        System.out.println(customer.myLongNumber);
        System.out.println(customer.myDoubleNumber);
        System.out.println(customer.myIntNumber);
        System.out.println(customer.myFloatNumber);
        System.out.println(customer.myLetter);
        System.out.println(customer.myBool);
        System.out.println("Run 5: ");
        myByte = 50;
        myShort = 2005;
        customer = new Customer(myByte, myShort,7005,16.9,951,16.51f,'E',false);
        System.out.println(customer.myByte);
        System.out.println(customer.myShortNumber);
        System.out.println(customer.myLongNumber);
        System.out.println(customer.myDoubleNumber);
        System.out.println(customer.myIntNumber);
        System.out.println(customer.myFloatNumber);
        System.out.println(customer.myLetter);
        System.out.println(customer.myBool);
    }
}
